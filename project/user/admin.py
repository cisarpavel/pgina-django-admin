#!/usr/bin/python
# -*- coding: utf8 -*-

from django.contrib import admin
from django.forms import widgets

from user.models import WindowsUser


class WindowsUserAdmin(admin.ModelAdmin):
    readonly_fields = tuple()
    fieldsets = (
        (None, {
            'fields': ('user', 'password')
        }),
        (u'Pokročilá nastavení', {
            'classes': ('collapse',),
            'fields': ('hash_method', )
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super(WindowsUserAdmin, self).get_readonly_fields(request, obj)
        if obj:
            readonly_fields += ('user',)
        return readonly_fields

    def save_model(self, request, obj, form, change):
        if form.has_changed() and 'password' in form.changed_data:
            obj.password = obj.get_hashed_password()

        obj.save()

    def get_form(self, request, obj=None, **kwargs):
        form_class = super(WindowsUserAdmin, self).get_form(request, obj, **kwargs)
        form_class.base_fields['password'].widget = widgets.PasswordInput(render_value=True)
        return form_class

admin.site.register(WindowsUser, WindowsUserAdmin)
