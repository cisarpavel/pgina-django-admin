#!/usr/bin/python
# -*- coding: utf8 -*-

import hashlib

from django.db import models
from django.utils.crypto import get_random_string

from user import choices as user_choices


class WindowsUser(models.Model):
    user = models.CharField(max_length=128, primary_key=True, verbose_name=u'uživatelské jméno')
    hash_method = models.CharField(max_length=16, choices=user_choices.HASH_METHOD_CHOICES, default=user_choices.HASH_METHOD_SSHA512, verbose_name=u'šifrování')
    password = models.CharField(max_length=2048, verbose_name=u'heslo')

    class Meta:
        db_table = 'users'
        verbose_name = u'Windows uživatel'
        verbose_name_plural = u'Windows uživatelé'

    def __unicode__(self):
        return u"%s" % self.user

    def get_hashed_password(self):
        if self.hash_method == user_choices.HASH_METHOD_NONE:
            return self.password
        elif self.hash_method in user_choices.SALTED_HASH_METHODS:
            salt = get_random_string(16)
            h = hashlib.new(self.hash_method.lower()[1:])
            h.update(self.password)
            h.update(salt)
            digest = h.digest()
            password = digest + salt
            return password.encode('hex')
        else:
            h = hashlib.new(self.hash_method.lower())
            h.update(self.password)
            digest = h.digest()
            return digest.encode('hex')
